module MyVars
  OS      = "ubuntu/precise64"
  OS_URL  = "https://atlas.hashicorp.com/ubuntu/boxes/precise64"
  PUPPET  = "scripts/upgrade-puppet.sh"
  RPM_URL  = ""
  RPM_NAME = ""
end
